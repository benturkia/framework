<?php

use App\Kernel;
use Symfony\Component\HttpFoundation\Request;

$loader = require __DIR__ . '/../app/autoload.php';

$kernel = new Kernel('prod', true);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);