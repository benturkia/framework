<?php

namespace App;

use App\Core\HttpKernel\Controller\ControllerResolver;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader as ContainerYamlFileLoader;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;
use Twig_Environment;
use Twig_Loader_Filesystem;

class Kernel
{
    /**
     * @var Container
     */
    protected $container;
    protected $rootDir;
    protected $environment;
    protected $debug;
    protected $booted = false;
    protected $name;
    protected $startTime;
    protected $loadClassCache;

    const NAME = 'Framework';
    const VERSION = '1.0.0-DEV';
    const VERSION_ID = 10000;
    const MAJOR_VERSION = 1;
    const MINOR_VERSION = 1;
    const RELEASE_VERSION = 1;
    const EXTRA_VERSION = 'DEV';

    /**
     * Constructor.
     *
     * @param string $environment The environment
     * @param bool   $debug       Whether to enable debugging or not
     */
    public function __construct($environment, $debug)
    {
        $this->environment = $environment;
        $this->debug = (bool) $debug;
        $this->rootDir = $this->getRootDir();
        $this->name = $this->getName();

        if ($this->debug) {
            $this->startTime = microtime(true);
        }
    }

    public function __clone()
    {
        if ($this->debug) {
            $this->startTime = microtime(true);
        }

        $this->booted = false;
        $this->container = null;
    }

    /**
     * Boots the current kernel.
     */
    public function boot()
    {
        if (true === $this->booted) {
            return;
        }

        // init container
        $this->initializeContainer();
        $this->initializeDoctrine();
        $this->initializeRouter();
        $this->initializeTwig();

        $this->booted = true;
    }

    /**
     * {@inheritdoc}
     */
    public function terminate(Request $request, Response $response)
    {
        if (false === $this->booted) {
            return;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function shutdown()
    {
        if (false === $this->booted) {
            return;
        }

        $this->booted = false;

        $this->container = null;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
        if (false === $this->booted) {
            $this->boot();
        }

        $dispatcher = new EventDispatcher();

        /** @var Router $router */
        $router = $this->getContainer()->get('router');
        $dispatcher->addSubscriber(new RouterListener($router->getMatcher(), new RequestStack()));

        $httpkernel = new HttpKernel($dispatcher, new ControllerResolver($this->getContainer()), new RequestStack(), new ArgumentResolver());

        return $httpkernel->handle($request);
    }

    protected function initializeContainer()
    {
        $container = new ContainerBuilder();

        $loader = new ContainerYamlFileLoader($container, new FileLocator($this->getRootDir() . '/Resources/config'));
        $loader->load('config_'. $this->getEnvironment() .'.yml');

        $container->set('kernel', $this);
        
        $this->container = $container;
    }
    
    protected function initializeDoctrine()
    {
        $config = new \Doctrine\DBAL\Configuration();

        $connectionParams = array(
            'driver'   => $this->getContainer()->getParameter('database_driver'),
            'host'   => $this->getContainer()->getParameter('database_host'),
            'user'     => $this->getContainer()->getParameter('database_user'),
            'password' => $this->getContainer()->getParameter('database_password'),
            'dbname'   => $this->getContainer()->getParameter('database_name'),
        );
        
        $connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

        $config = Setup::createAnnotationMetadataConfiguration(
            array($this->getRootDir() . '/Entity'),
            $this->isDebug(),
            null,
            new FilesystemCache($this->getCacheDir()),
            false
        );

        $entityManager = EntityManager::create($connectionParams, $config);
        
        $this->getContainer()->set('doctrine.connection', $connection);
        $this->getContainer()->set('doctrine.entity_manager', $entityManager);
    }

    protected function initializeTwig()
    {
        $loader = new Twig_Loader_Filesystem($this->getRootDir() . '/Resources/views');
        $config = array(
            'debug' => $this->isDebug()
        );

        if ($this->getEnvironment() == 'prod')
        {
            $config['cache'] = $this->getCacheDir() . '/twig';
        }

        $twig = new Twig_Environment($loader, $config);


        $this->container->set('twig', $twig);
    }

    protected function initializeRouter()
    {
        $config = array();
        $locator = new FileLocator(array(__DIR__));

        if ($this->getEnvironment() == 'prod') {
            $config['cache_dir'] = $this->getCacheDir();
        }

        $router = new Router(
            new YamlFileLoader($locator),
            $this->getRootDir() . '/Resources/config/routing/routes.yml',
            $config,
            new RequestContext('/')
        );

        $this->container->set('router', $router);
    }

    /**
     * @return ContainerBuilder
     */
    public function getContainer()
    {
        return $this->container;
    }

    public function getName()
    {
        return self::NAME;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname($this->getRootDir()) . '/var/cache/' . $this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname($this->getRootDir()) . '/var/logs';
    }

    /**
     * {@inheritdoc}
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * {@inheritdoc}
     */
    public function isDebug()
    {
        return $this->debug;
    }

    public function isBooted()
    {
        return $this->booted;
    }

    public function getVersion()
    {
        return self::VERSION;
    }
}