<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PageController extends Controller
{
    public function indexAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('page/home.html.twig', compact('users'));
    }

    public function contactAction()
    {
        $response = new Response();

        $response->setContent('contact');

        return $response;
    }
}