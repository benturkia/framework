<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

abstract class Controller implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param $name
     *
     * @return object
     */
    public function get($name)
    {
        return $this->getContainer()->get($name);
    }

    public function renderView($template, $data = array())
    {
        /** @var \Twig_Environment $twig */
        $twig = $this->get('twig');

        $template = $twig->load($template);

        return $template->render($data);
    }

    public function render($template, $data = array())
    {
        $response = new Response();
        $response->setContent($this->renderView($template, $data));

        return $response;
    }

    /**
     * @return EntityManager
     */
    public function getDoctrine()
    {
        return $this->get('doctrine.entity_manager');
    }
}