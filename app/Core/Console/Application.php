<?php

namespace App\Core\Console;

use App\Kernel;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class Application extends ConsoleApplication
{
    /**
     * @var Kernel
     */
    private $kernel;

    public function __construct(Kernel $kernel)
    {
        $this->kernel = $kernel;

        parent::__construct($kernel->getName(), $kernel->getVersion());

        $this->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The environment name', $kernel->getEnvironment()));
    }

    /**
     * Gets the Kernel associated with this Console.
     *
     * @return Kernel A KernelInterface instance
     */
    public function getKernel()
    {
        return $this->kernel;
    }

    public function doRun(InputInterface $input, OutputInterface $output)
    {
        $this->kernel->boot();
        $container = $this->kernel->getContainer();

        foreach ($this->all() as $command) {
            if ($command instanceof ContainerAwareInterface) {
                $command->setContainer($container);
            }
        }

        return parent::doRun($input, $output);
    }

    /**
     * {@inheritdoc}
     */
    public function all($namespace = null)
    {
        $this->registerCommands();
        return parent::all($namespace);
    }

    protected function registerCommands()
    {
        $container = $this->kernel->getContainer();

        foreach ($container->findTaggedServiceIds('console.command') as $id => $service) {
            $this->add($container->get($id));
        }
    }
}